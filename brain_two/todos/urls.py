from django.urls import path
from todos.views import (
    todo_list_list,
    todo_list_detail,
    create_list,
    edit_list,
    delete_list_item,
)

urlpatterns = [
    path("", todo_list_list, name="todos_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", create_list, name="create_list"),
    path("<int:id>/edit/", edit_list, name="edit_list"),
    path("<int:id>/delete/", delete_list_item, name="delete_list_item"),
]

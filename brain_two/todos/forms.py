from todos.models import TodoList
from django.forms import ModelForm


class TodoForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ("name",)


class TaskForm(ModelForm):
    class Meta:
        model = TodoList
        fields = (
            "Task",
            "Due Date",
            "Is Completed",
            "List",
        )

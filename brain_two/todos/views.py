from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    count = TodoList.objects.count()
    context = {
        "todo_list": todos,
        "count": count,
    }
    return render(request, "todos/todos.html", context)


def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": detail,
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_object = form.save(False)
            todo_object.save()
            return redirect("todo_list_detail", id=todo_object.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_list(request, id):
    todo_object = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_object)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)
    else:
        form = TodoForm(instance=todo_object)
    context = {"form": form}
    return render(request, "todos/edit.html", context)


def delete_list_item(request, id):
    instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        instance.delete()
        return redirect("todos_list")
    return render(request, "todos/delete.html")


def item_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect(
                "todos/todo_item_create.html", id=model_instance.list.id
            )
    else:
        form = TodoForm()

    context = {
        "form": form,
    }

    return render(request, "create.html/", context)
